using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.DTOs
{
    public class OperationsHistoryWithUser
    {
        public OperationsHistory OperationsHistory { get; set; }
        public User User { get; set; }
    }
}