using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.DTOs
{
    public class AccountWithHistoryDto
    {
        public Account Account { get; set; }
        public IEnumerable<OperationsHistory> Histories { get; set; }
    }
}