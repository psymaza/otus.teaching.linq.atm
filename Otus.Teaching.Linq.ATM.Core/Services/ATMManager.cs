﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Otus.Teaching.Linq.ATM.Core.DTOs;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users,
            IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetAccountInfo(string userName, string password)
            => Users.SingleOrDefault(e => e.Login.Equals(userName) && e.Password.Equals(password))
               ?? GetDefaultUser();

        public IList<Account> GetUserAccounts(User user)
            => Accounts.Where(e => e.UserId.Equals(user.Id))
                .ToList();

        public IList<AccountWithHistoryDto> GetUserAccountsWithHistory(User user)
            => GetUserAccounts(user)
                .GroupJoin(History,
                    account => account.Id,
                    history => history.AccountId,
                    (account, histories) => new AccountWithHistoryDto
                    {
                        Account = account,
                        Histories = histories
                    }
                )
                .ToList();

        public IList<OperationsHistoryWithUser> GetOperationsHistoriesWithUser(OperationType? operationType = null)
            => History
                .Where(e => operationType == null || e.OperationType.Equals(operationType))
                .Join(Accounts,
                    history => history.AccountId,
                    account => account.Id,
                    (history, account) => new {history, account.UserId}
                )
                .Join(Users,
                    query => query.UserId,
                    user => user.Id,
                    (query, user) => new OperationsHistoryWithUser
                    {
                        User = user,
                        OperationsHistory = query.history
                    }
                )
                .ToList();

        public IList<User> GetUsersByAmountGreaterThan(decimal amount)
            => Users
                .Join(Accounts.Where(e => e.CashAll > amount),
                    user => user.Id,
                    account => account.UserId,
                    (user, account) => user
                ).ToList();

        private User GetDefaultUser()
        {
            var defaultValue = "Unknown";
            return new User
            {
                Login = defaultValue,
                Password = string.Empty,
                FirstName = defaultValue,
                MiddleName = defaultValue,
                Phone = defaultValue,
                SurName = defaultValue,
                PassportSeriesAndNumber = defaultValue,
                RegistrationDate = DateTime.Now
            };
        }
    }
}