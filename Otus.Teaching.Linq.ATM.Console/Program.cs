﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    static class Program
    {
        private static string _delimeter = new string('=', 5);

        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            atmManager.Users
                .Select(e => atmManager.GetAccountInfo(e.Login, e.Password))
                .Select(e => $"{e.FirstName} {e.MiddleName} {e.SurName}")
                .Print("Users");

            atmManager.Users
                .Select(e => new {user = e, accounts = atmManager.GetUserAccounts(e)})
                .SelectMany(e => e.accounts.Select(acc => $"{e.user.FirstName}: {acc.CashAll}; {acc.OpeningDate}"))
                .Print("Accounts");

            atmManager.Users
                .Select(e => new {user = e, accounts = atmManager.GetUserAccountsWithHistory(e)})
                .SelectMany(e => e.accounts.SelectMany(acc => acc.Histories.Select(history =>
                    $"{e.user.FirstName}: {acc.Account.CashAll}; {acc.Account.OpeningDate}; {(history.OperationType == OperationType.InputCash ? "Поступление" : "Расход")} : {history.CashSum}")))
                .Print("History");
            
            atmManager.GetOperationsHistoriesWithUser(OperationType.InputCash)
                .Select(e => $"{e.User.FirstName}: {e.OperationsHistory.CashSum} - {e.OperationsHistory.OperationDate}")
                .Print("Input operations");

            var greaterThan = 100000;
            atmManager.GetUsersByAmountGreaterThan(greaterThan)
                .Select(e => $"{e.FirstName} {e.MiddleName} {e.SurName}")
                .Print($"Greater than: {greaterThan}");

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static void Print(this IEnumerable<string> collections, string title = null)
        {
            if (!string.IsNullOrEmpty(title))
                System.Console.WriteLine($"{_delimeter} {title} {_delimeter}");

            System.Console.WriteLine(string.Join(Environment.NewLine, collections));
            System.Console.WriteLine();
        }


        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}